NODECOUNT = 5
k8s_host_tpl = 
k8s_cluster_ip_tpl = "192.168.127.%s"
k8s_master_ip = k8s_cluster_ip_tpl % "10"

# (1..NODECOUNT).each do |i|
#     puts k8s_cluster_ip_tpl % (10 + i)
# end

WORKER_NODECOUNT = 2
MASTER_CONSTRUCTS = {"name": "master", "ip": k8s_master_ip}
WORKER_CONSTRUCTS = (1..WORKER_NODECOUNT).map { |n| { "name": "worker#{n}", "ip": k8s_cluster_ip_tpl % (10 + n)} }
puts MASTER_CONSTRUCTS[:ip]


puts "MASTER_CONSTRUCTS = #{MASTER_CONSTRUCTS}"
puts "WORKER_CONSTRUCTS = #{WORKER_CONSTRUCTS}"

CONCAT = [MASTER_CONSTRUCTS] + WORKER_CONSTRUCTS
puts "CONCAT = #{CONCAT}"

WORKER_CONSTRUCTS.each do |worker_node|
    puts worker_node[:ip]
    puts worker_node[:name]
end

  # - { name: vg-k8s-master, ip: 172.16.77.10 }
  # - { name: vg-k8s-node-1, ip: 172.16.77.11 }
  # - { name: vg-k8s-node-2, ip: 172.16.77.12 }
  # - { name: vg-k8s-node-3, ip: 172.16.77.13 }
