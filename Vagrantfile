# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

VM_CPUS = 2
VM_MEMORY = 4096
IMAGE_NAME = "centos/cn"
IMAGE_URL = "https://mirrors.ustc.edu.cn/centos-cloud/centos/7/vagrant/x86_64/images"

k8s_cluster_ip_tpl = "192.168.127.%s"
k8s_master_ip = k8s_cluster_ip_tpl % "10"

WORKER_NODECOUNT=2
MASTER_CONSTRUCTS = {"name": "master", "ip": k8s_master_ip}
WORKER_CONSTRUCTS = (1..WORKER_NODECOUNT).map { |n| { "name": "worker#{n}", "ip": k8s_cluster_ip_tpl % (10 + n)} }

puts "******** K8s Vagrant CENTOS setup starts ********"
puts ""
puts "MASTER_CONSTRUCTS = #{MASTER_CONSTRUCTS}"
puts "WORKER_CONSTRUCTS = #{WORKER_CONSTRUCTS}"

Vagrant.configure("2") do |config|
  config.vm.define MASTER_CONSTRUCTS[:name], primary: true do |master|
    puts "******** Setting up master #{MASTER_CONSTRUCTS[:name]}********"
    puts ""
    master.vm.box = IMAGE_NAME
    master.vm.box_url = IMAGE_URL
    master.vm.box_check_update = false
    master.vm.hostname = MASTER_CONSTRUCTS[:name]
    master.vm.network "private_network", ip: MASTER_CONSTRUCTS[:ip]
    master.vm.network "public_network",  bridge: "Intel(R) Wi-Fi 6 AX200 160MHz"

    master.vm.provider "virtualbox" do |vb|
      vb.name = MASTER_CONSTRUCTS[:name]
      vb.memory = VM_MEMORY
      vb.cpus = VM_CPUS
      vb.gui = false
      vb.linked_clone = true
      vb.customize ["modifyvm", :id, "--vram", "8"] 
    end

    master.vm.provision :ansible_local do |ansible|
      ansible.playbook = "k8s-setup-script/common-setup.playbook.yml"
      ansible.compatibility_mode = "2.0"
      ansible.extra_vars = {
        node_ip: MASTER_CONSTRUCTS[:ip],
        host_group: [MASTER_CONSTRUCTS] + WORKER_CONSTRUCTS
      }
    end
  end

  WORKER_CONSTRUCTS.each do |worker_node|
    puts "******** Setting up master #{worker_node[:name]}********"
    puts ""
    config.vm.define worker_node[:name] do |worker|
      worker.vm.box = IMAGE_NAME
      worker.vm.box_url = IMAGE_URL
      worker.vm.box_check_update = false
      worker.vm.hostname = worker_node[:name]
      worker.vm.network "private_network", ip: worker_node[:ip]
      worker.vm.network "public_network",  bridge: "Intel(R) Wi-Fi 6 AX200 160MHz"
      worker.vm.provider "virtualbox" do |vb|
        vb.name = worker_node[:name]
        vb.memory = VM_MEMORY
        vb.cpus = VM_CPUS
          vb.gui = false
          vb.linked_clone = true
          vb.customize ["modifyvm", :id, "--vram", "8"] 
      end
  
      worker.vm.provision :ansible_local do |ansible|
          ansible.playbook = "k8s-setup-script/common-setup.playbook.yml"
          ansible.compatibility_mode = "2.0"
          ansible.extra_vars = {
            node_ip: worker_node[:ip],
            host_group: [MASTER_CONSTRUCTS] + WORKER_CONSTRUCTS
          }
      end
    end
  end

  vagrant_synced_folder_default_type = ""
end
